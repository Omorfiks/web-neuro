import cv2
from keras.models import load_model
import numpy as np
import math
model = load_model('facial_recognition_model.h5')
photo = str(input("Укажите путь к фото: "))
width = 96
height = 96
dsize = (width, height)

while True:
    img = cv2.imread(photo)
    img = cv2.resize(img, dsize)
    image_list = np.array(img, dtype=float)
    image_list = image_list.reshape(-1,96,96,1)
    prediction = [list(map(str, i)) for i in model.predict(image_list)]
    prediction = (''.join(map(str,prediction)))
    

    break
print(prediction)
cv2.imshow(str(photo),img)
cv2.waitKey(0)