Заходим на тестовый стенд 
https://labs.play-with-docker.com

Слева
add insrtance

Копируем ветку dev c архивом
git clone --branch dev https://gitlab.com/Omorfiks/web-neuro.git

Разаривируем все наше богатство
Устанавливаем npm если необходимо разрабатывать прям в этой машинке
`apk add npm`

проинициализировал новый проект
`npm init vue@latest`

Создал dockerfile
`touch Dockerfile`

добавил содержимое для деплоя

```console
FROM node:lts-alpine

# устанавливаем простой HTTP-сервер для статики
RUN npm install -g http-server

# делаем каталог 'app' текущим рабочим каталогом
WORKDIR /app

# копируем оба 'package.json' и 'package-lock.json' (если есть)
COPY package*.json ./

# устанавливаем зависимости проекта
RUN npm install

# копируем файлы и каталоги проекта в текущий рабочий каталог (т.е. в каталог 'app')
COPY . .

# собираем приложение для production с минификацией
RUN npm run build

EXPOSE 8080
CMD [ "http-server", "dist" ]
```

собираем фронт

`docker build -t test .`

запускаем фронт
`docker run -itd -p 8080:8080 --rm --name one test`

# push

```consale
$ git add .
[node1] (local) root@192.168.0.28 ~/web-neuro
$ git commit -m "simple project"
Author identity unknown

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: unable to auto-detect email address (got 'root@node1.(none)')
[node1] (local) root@192.168.0.28 ~/web-neuro
$ git config --global user.email "you@example.com"
[node1] (local) root@192.168.0.28 ~/web-neuro
$ git config --global user.name "Your Name"
[node1] (local) root@192.168.0.28 ~/web-neuro
$ git commit -m "simple project"
[dev 3e4e65c] simple project
 22 files changed, 1764 insertions(+)
 create mode 100644 web/.gitignore
 create mode 100644 web/.vscode/extensions.json
 create mode 100644 web/Dockerfile
 create mode 100644 web/README.md
 create mode 100644 web/index.html
 create mode 100644 web/package-lock.json
 create mode 100644 web/package.json
 create mode 100644 web/public/favicon.ico
 create mode 100644 web/src/App.vue
 create mode 100644 web/src/assets/base.css
 create mode 100644 web/src/assets/logo.svg
 create mode 100644 web/src/assets/main.css
 create mode 100644 web/src/components/HelloWorld.vue
 create mode 100644 web/src/components/TheWelcome.vue
 create mode 100644 web/src/components/WelcomeItem.vue
 create mode 100644 web/src/components/icons/IconCommunity.vue
 create mode 100644 web/src/components/icons/IconDocumentation.vue
 create mode 100644 web/src/components/icons/IconEcosystem.vue
 create mode 100644 web/src/components/icons/IconSupport.vue
 create mode 100644 web/src/components/icons/IconTooling.vue
 create mode 100644 web/src/main.js
 create mode 100644 web/vite.config.js
[node1] (local) root@192.168.0.28 ~/web-neuro
$ git push
Username for 'https://gitlab.com': antonholmes
Password for 'https://antonholmes@gitlab.com': 
Enumerating objects: 32, done.
Counting objects: 100% (32/32), done.
Delta compression using up to 8 threads
Compressing objects: 100% (29/29), done.
Writing objects: 100% (31/31), 16.08 KiB | 2.01 MiB/s, done.
Total 31 (delta 0), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for dev, visit:
remote:   https://gitlab.com/Omorfiks/web-neuro/-/merge_requests/new?merge_request%5Bsource_branch%5D=dev
remote: 
To https://gitlab.com/Omorfiks/web-neuro.git
   5ad8f4f..3e4e65c  dev -> dev
```


# Запуск

для запуска на тестовом стенде как в локальной среде

Копируем проект с нужной веткой
`git clone` 

собираем фронт

`docker build -t test .`

запускаем фронт
`docker run -itd -p 8080:8080 --rm --name one test`
